#!/bin/bash
DOCKER_IMAGE="registry.gitlab.com/raniere-phd/quarto-document-template"
docker compose build web --pull --no-cache --progress plain
docker image tag quarto-document-template_web $DOCKER_IMAGE
docker push -q $DOCKER_IMAGE
